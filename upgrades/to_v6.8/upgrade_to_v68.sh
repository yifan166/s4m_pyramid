#!/bin/bash

cd /data/repo/git-working/stemformatics/upgrades/to_v6.8/
./collapse_v68_scripts.sh


psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'v6.8' where ref_type = 'stemformatics_version';"
psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'true' where ref_type = 'use_cdn';"
psql -U portaladmin portal_prod -c "update stemformatics.configs set ref_id = 'stemformatics.sa.metacdn.com/release_68' where ref_type = 'cdn_base_url';"
# delete old PCA records from dataset_metadata
psql -U portaladmin portal_prod -c "delete from dataset_metadata where ds_name = 'showReportOnDatasetSummaryPage' and ds_value ilike '%PCA%';"

wget http://localhost:5000/api/trigger_config_update -O /dev/null


echo "Now you will need to upgrade the help to v6.8"
