-- add config items for Graph Integration

insert into stemformatics.configs (ref_type,ref_id) values ('expiry_time',86400); 

insert into stemformatics.configs (ref_type,ref_id) values('graph_colours','["red", "Orange", "DodgerBlue", "Blue","BlueViolet","Brown", "Deeppink", "BurlyWood","CadetBlue","Chartreuse","Chocolate","Coral","CornflowerBlue","Crimson","Cyan", "Red", "DarkBlue","DarkGoldenRod","DarkGray", "Tomato", "Violet","DarkGreen","DarkKhaki","DarkMagenta","DarkOliveGreen","DarkOrange","DarkOrchid","DarkRed","DarkSalmon","DarkSlateBlue","DarkTurquoise","DarkViolet","DeepPink","DeepSkyBlue","DodgerBlue","FireBrick","ForestGreen","Fuchsia","Gold","GoldenRod","Green","GreenYellow","HotPink","IndianRed","Indigo"]'); 

insert into stemformatics.configs (ref_type,ref_id) values ('sample_type_colours','["#9932cc","#FFA500","#1e90ff","#0000FF","#A52A2A","#8B008B","#ff1493","#b0e0e6","#deb887","#5f9ea0","#99FF00","#d2691e","#ff7f50","#6495ed","#990000","#00FFFF","#FF0000","#483d8b","#b8860b","#A9A9A9","#FF6347","#EE82EE","#000080","#006400","#bdb76b","#FF00FF","#556b2f","#ee7600","#8B0000","#e9967a","#483d8b","#00ced1","#9400D3","#00bfff","#1e90ff","#b22222","#228b22","#FF00FF","#FFD700","#daa520","#008000","#adff2f","#FF69B4","#cd5c5c","#4B0082"]'); 
