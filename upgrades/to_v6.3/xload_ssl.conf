#
#   Some MIME-types for downloading Certificates and CRLs
#
AddType application/x-x509-ca-cert .crt
AddType application/x-pkcs7-crl    .crl

##
## SSL Virtual Host Context
##

<VirtualHost www2.stemformatics.org:443>

# General setup for the virtual host, inherited from global configuration
#DocumentRoot "/var/www/html"
ServerName www2.stemformatics.org:443

# Use separate log files for the SSL virtual host; note that LogLevel
# is not inherited from httpd.conf.
ErrorLog logs/ssl_error_log
TransferLog logs/ssl_access_log
LogLevel warn

#   SSL Engine Switch:
#   Enable/Disable SSL for this virtual host.
SSLEngine on

#   SSL Protocol support:
# List the enable protocol levels with which clients will be able to
# connect.  Disable SSLv2 access by default:
SSLProtocol all -SSLv2 -SSLv3 -TLSv1
SSLCipherSuite ALL:!ADH:!EXPORT:!SSLv2:!SSLv3:RC4+RSA:+HIGH:+MEDIUM:+LOW

#   SSL Cipher Suite:
# List the ciphers that the client is permitted to negotiate.
# See the mod_ssl documentation for a complete list.

#   Server Certificate:
# Point SSLCertificateFile at a PEM encoded certificate.  If
# the certificate is encrypted, then you will be prompted for a
# pass phrase.  Note that a kill -HUP will prompt again.  A new
# certificate can be generated using the genkey(1) command.
SSLCertificateFile /etc/httpd/cert/stemformatics.org.crt
#SSLCertificateFile /etc/httpd/cert/cert_selfsigned.crt

#   Server Private Key:
#   If the key is not combined with the certificate, use this
#   directive to point at the key file.  Keep in mind that if
#   you've both a RSA and a DSA private key you can configure
#   both in parallel (to also allow the use of DSA ciphers, etc.)
SSLCertificateKeyFile /etc/httpd/cert/stemformatics.key
#SSLCertificateKeyFile /etc/httpd/cert/cert_selfsigned.key

#   Server Certificate Chain:
#   Point SSLCertificateChainFile at a file containing the
#   concatenation of PEM encoded CA certificates which form the
#   certificate chain for the server certificate. Alternatively
#   the referenced file can be the same as SSLCertificateFile
#   when the CA certificates are directly appended to the server
#   certificate for convinience.
SSLCertificateChainFile /etc/httpd/cert/sf_bundle.crt

#   Certificate Authority (CA):
#   Set the CA certificate verification path where to find CA
#   certificates for client authentication or alternatively one
#   huge file containing all of them (file must be PEM encoded)
#SSLCACertificateFile /etc/pki/tls/certs/ca-bundle.crt

#   Client Authentication (Type):
#   Client certificate verification type and depth.  Types are
#   none, optional, require and optional_no_ca.  Depth is a
#   number which specifies how deeply to verify the certificate
#   issuer chain before deciding the certificate is not valid.
#SSLVerifyClient require
#SSLVerifyDepth  10

#   Access Control:
#   With SSLRequire you can do per-directory access control based
#   on arbitrary complex boolean expressions containing server
#   variable checks and other lookup directives.  The syntax is a
#   mixture between C and Perl.  See the mod_ssl documentation
#   for more details.
#<Location />
#SSLRequire (    %{SSL_CIPHER} !~ m/^(EXP|NULL)/ \
#            and %{SSL_CLIENT_S_DN_O} eq "Snake Oil, Ltd." \
#            and %{SSL_CLIENT_S_DN_OU} in {"Staff", "CA", "Dev"} \
#            and %{TIME_WDAY} >= 1 and %{TIME_WDAY} <= 5 \
#            and %{TIME_HOUR} >= 8 and %{TIME_HOUR} <= 20       ) \
#           or %{REMOTE_ADDR} =~ m/^192\.76\.162\.[0-9]+$/
#</Location>

#   SSL Engine Options:
#   Set various options for the SSL engine.
#   o FakeBasicAuth:
#     Translate the client X.509 into a Basic Authorisation.  This means that
#     the standard Auth/DBMAuth methods can be used for access control.  The
#     user name is the `one line' version of the client's X.509 certificate.
#     Note that no password is obtained from the user. Every entry in the user
#     file needs this password: `xxj31ZMTZzkVA'.
#   o ExportCertData:
#     This exports two additional environment variables: SSL_CLIENT_CERT and
#     SSL_SERVER_CERT. These contain the PEM-encoded certificates of the
#     server (always existing) and the client (only existing when client
#     authentication is used). This can be used to import the certificates
#     into CGI scripts.
#   o StdEnvVars:
#     This exports the standard SSL/TLS related `SSL_*' environment variables.
#     Per default this exportation is switched off for performance reasons,
#     because the extraction step is an expensive operation and is usually
#     useless for serving static content. So one usually enables the
#     exportation for CGI and SSI requests only.
#   o StrictRequire:
#     This denies access when "SSLRequireSSL" or "SSLRequire" applied even
#     under a "Satisfy any" situation, i.e. when it applies access is denied
#     and no other module can change it.
#   o OptRenegotiate:
#     This enables optimized SSL connection renegotiation handling when SSL
#     directives are used in per-directory context. 
#SSLOptions +FakeBasicAuth +ExportCertData +StrictRequire
<Files ~ "\.(cgi|shtml|phtml|php3?)$">
    SSLOptions +StdEnvVars
</Files>
<Directory "/var/www/cgi-bin">
    SSLOptions +StdEnvVars
</Directory>

#   SSL Protocol Adjustments:
#   The safe and default but still SSL/TLS standard compliant shutdown
#   approach is that mod_ssl sends the close notify alert but doesn't wait for
#   the close notify alert from client. When you need a different shutdown
#   approach you can use one of the following variables:
#   o ssl-unclean-shutdown:
#     This forces an unclean shutdown when the connection is closed, i.e. no
#     SSL close notify alert is send or allowed to received.  This violates
#     the SSL/TLS standard but is needed for some brain-dead browsers. Use
#     this when you receive I/O errors because of the standard approach where
#     mod_ssl sends the close notify alert.
#   o ssl-accurate-shutdown:
#     This forces an accurate shutdown when the connection is closed, i.e. a
#     SSL close notify alert is send and mod_ssl waits for the close notify
#     alert of the client. This is 100% SSL/TLS standard compliant, but in
#     practice often causes hanging connections with brain-dead browsers. Use
#     this only for browsers where you know that their SSL implementation
#     works correctly. 
#   Notice: Most problems of broken clients are also related to the HTTP
#   keep-alive facility, so you usually additionally want to disable
#   keep-alive for those clients, too. Use variable "nokeepalive" for this.
#   Similarly, one has to force some clients to use HTTP/1.0 to workaround
#   their broken HTTP/1.1 implementation. Use variables "downgrade-1.0" and
#   "force-response-1.0" for this.
SetEnvIf User-Agent ".*MSIE.*" \
         nokeepalive ssl-unclean-shutdown \
         downgrade-1.0 force-response-1.0

#   Per-Server Logging:
#   The home of a custom SSL log file. Use this when you want a
#   compact non-error SSL logfile on a virtual host basis.
#CustomLog logs/ssl_request_log \
#          "%t %h %{SSL_PROTOCOL}x %{SSL_CIPHER}x \"%r\" %b"


### STEMFORMATICS APPLICATION CONFIGURATION ###

   AddType application/octet-stream .gct
   #AddType application/octet-stream .txt
   AddType application/octet-stream .cls

   ## Rewrite our ".com" and ".net" URLs to ".org"
   <IfModule mod_rewrite.c>
     RewriteEngine on
     ## Block external requets for API path.
     ## Task #186 agile_org
     RewriteRule ^/api - [R=403,L]
     ## Block external requests for Affymetrix CDF data bundle
     RewriteRule ^/downloads/s4mdata\.tgz - [R=403,L]

     RewriteCond %{HTTP_HOST} ^stemformatics.com
     RewriteRule ^/(.*)$ https://stemformatics.org/$1 [R=permanent,L]
     RewriteCond %{HTTP_HOST} ^www.stemformatics.com
     RewriteRule ^/(.*)$ https://stemformatics.org/$1 [R=permanent,L]
     RewriteCond %{HTTP_HOST} ^stemformatics.net
     RewriteRule ^/(.*)$ https://stemformatics.org/$1 [R=permanent,L]
     RewriteCond %{HTTP_HOST} ^www.stemformatics.net
     RewriteRule ^/(.*)$ https://stemformatics.org/$1 [R=permanent,L]

     ## If coming from old cache or Google, serve up latest protocols doc.
     RewriteRule  ^(.*)data_normalisation_and_transformation\.pdf $1data_normalization_and_transformation_v2\.pdf [R=permanent,L]
     RewriteRule  ^(.*)data_normalization_and_transformation_v2\.pdf $1Stemformatics_data_methods\.pdf [R=permanent,L]
   </IfModule>

   <Location "/" >
      Order deny,allow
      allow from all
   </Location>

   <IfModule mod_proxy.c>
      ProxyRequests off
      ProxyPreserveHost on

      ## Don't proxy downloads
      ProxyPassMatch /mappings/ !
      ProxyPassMatch /downloads/ !
      ProxyPassMatch /images/ !
      ProxyPassMatch /img/ !
      ProxyPassMatch /js/ !
      ProxyPassMatch /help/ !
      ProxyPassMatch /css/ !
      ProxyPassMatch /themes/ !

      ## Paster/Pylons transparent proxy for Stem Cell Data Portal
      ProxyPassMatch /(.*)$ http://127.0.0.1:5000/$1
      ProxyPassReverse / http://127.0.0.1:5000
   </IfModule>

   ErrorLog  /var/log/httpd/portal_error_log
   CustomLog /var/log/httpd/portal_access_log combined

   ## T#2408: Force probe mappings .txt files to TSV content type
   <FilesMatch "mapping_.*\.txt$">
     Header set Content-Type "text/tab-separated-values;"
   </FilesMatch>

</VirtualHost>                                  

