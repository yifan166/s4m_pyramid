#!/bin/bash

psql -U portaladmin portal_prod -f 2626_copy_over_assay_platform_fields_to_metastore.sql
source /var/www/pylons/virtualenv/bin/activate
python 2626_copy_over_assay_platform_fields_to_metastore.py


# Now also reset all the redis dataset metadata
redis-cli -s /data/redis/redis.sock KEYS "dataset_metadata|*" | xargs redis-cli -s /data/redis/redis.sock DEL
