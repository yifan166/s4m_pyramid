alter table assay_platforms rename column species to z_species;
alter table assay_platforms rename column platform_type to z_platform_type;
alter table assay_platforms rename column min_y_axis to z_min_y_axis;
alter table assay_platforms rename column y_axis_label to z_y_axis_label;
alter table assay_platforms rename column y_axis_label_description to z_y_axis_label_description;
alter table assay_platforms rename column mapping_id to z_mapping_id;
alter table assay_platforms rename column log_2 to z_log_2;
alter table assay_platforms rename column probe_name to z_probe_name;
alter table assay_platforms rename column default_graph_title to z_default_graph_title;

