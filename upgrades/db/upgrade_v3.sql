

-- Task #240 feature mappings table
CREATE TABLE stemformatics.feature_mappings (
    db_id INT REFERENCES annotation_databases (an_database_id) NOT NULL,
    chip_type INT REFERENCES assay_platforms (chip_type) NOT NULL,
    from_type TEXT NOT NULL,
    from_id TEXT NOT NULL,
    to_type TEXT NOT NULL,
    to_id TEXT NOT NULL,
    PRIMARY KEY (db_id, chip_type,from_type,from_id,to_type,to_id)
);

CREATE INDEX to_index on stemformatics.feature_mappings (db_id,chip_type,to_type,to_id);
    
-- end task 240



-- Story #158 add email fields for users as well as Task #258 ( default of true)

ALTER TABLE stemformatics.users ADD COLUMN send_email_marketing boolean NOT NULL default True;
ALTER TABLE stemformatics.users ADD COLUMN send_email_job_notifications boolean NOT NULL default True;

-- end story #158




-- task #195 finished timestamp used for removing jobs for cron purposes
ALTER TABLE stemformatics.jobs ADD COLUMN finished timestamp without time zone default NULL;
-- update the current jobs to have the finished set to be 10 minutes after created if their status is completed (1)
update stemformatics.jobs set finished = created + interval '10 minutes' where status = 1;


-- Task #223 add published column to hide datasets
ALTER TABLE public.datasets ADD COLUMN published boolean NOT NULL default False;




-- Story #165 override unpublished
CREATE TABLE stemformatics.override_private_datasets (
    ds_id INT REFERENCES datasets (id) NOT NULL,
    uid INT REFERENCES stemformatics.users (uid) NOT NULL,
    role TEXT NOT NULL default 'view',
    PRIMARY KEY (uid,ds_id)
);
CREATE INDEX ds_id_index ON stemformatics.override_private_datasets (ds_id);
ALTER TABLE public.datasets ADD COLUMN private boolean NOT NULL default False;
-- Adding in an override user dataset,uid,role
-- insert into stemformatics.override_private_datasets values(1000,6,'view');


-- ----------------------------Story #159 Gene Set Annotations --------------------------------


-- Story #159 for annotating gene sets
CREATE TABLE stemformatics.transcript_annotations
(
   db_id INT REFERENCES annotation_databases (an_database_id),
   transcript_id text NOT NULL,
   gene_id text NOT NULL,
   transcript_name text NOT NULL,
   transcript_start integer NOT NULL,
   transcript_end integer NOT NULL,
   protein_length integer NOT NULL default 0,
   signal_peptide boolean default False,
   tm_domain boolean default False,
   targeted_miRNA boolean default False,
   CONSTRAINT transcript_annotations_pkey PRIMARY KEY (db_id, transcript_id)
);


CREATE INDEX transcript_annotations_db_id_gene_id ON stemformatics.transcript_annotations (db_id, gene_id);


-- Story #159 for saving filters for annotations
CREATE TABLE stemformatics.annotation_filters
(
   id serial NOT NULL,
   uid INT NOT NULL,
   name text NOT NULL,
   json_filter text NOT NULL,
   CONSTRAINT annotation_filters_pkey PRIMARY KEY (id)
);

CREATE INDEX annotation_filters_uid ON stemformatics.annotation_filters (uid);



update stemformatics.transcript_annotations set size = transcript_end - transcript_start;




-- ensure gene set items has index on gene id
CREATE INDEX gene_set_items_gene_id ON stemformatics.gene_set_items (gene_id);


-- have to remove transcript start/end/size details as they are useless
alter table stemformatics.transcript_annotations drop column transcript_start;
alter table stemformatics.transcript_annotations drop column transcript_end;


-- gene sets adding the type 
ALTER TABLE stemformatics.gene_sets ADD COLUMN gene_set_type text NOT NULL default '';



-- speeding up the annotation page
CREATE INDEX gene_sets_uid_id ON stemformatics.gene_sets (uid,id);



-- Task #187 gene set name should be per database id (species)
-- this is so we can have the same kegg pathway name for human and mouse
ALTER TABLE stemformatics.gene_sets DROP CONSTRAINT unique_gene_sets;
ALTER TABLE stemformatics.gene_sets ADD CONSTRAINT unique_gene_sets UNIQUE (gene_set_name,uid,db_id);


-- WARNING
-- WARNING
-- WARNING
-- WARNING
-- WARNING
-- WARNING
-- WARNING

-- this is to be used only if you cannot upload tsv because uid is before description

-- alter table stemformatics.gene_sets add column new_uid bigint;
-- update stemformatics.gene_sets set new_uid = uid;
-- alter table stemformatics.gene_sets drop column uid;
-- alter table stemformatics.gene_sets rename column new_uid to uid;
