-- SQL upgrades for v3.4

ALTER TABLE biosamples_metadata ADD COLUMN ds_id INT NOT NULL default 0;
CREATE INDEX ds_id_chip_id on biosamples_metadata (ds_id,chip_id);

-- update the biosamples_metadata to have a ds_id based on the biosamples
update biosamples_metadata set ds_id = biosamples.ds_id from biosamples where biosamples.chip_id = biosamples_metadata.chip_id;

-- this was for testing only on Rowland's local postgres database
-- insert into biosamples (chip_id, chip_type, ds_id,alias,archive_accession_id,sample_id) select chip_id,chip_type,2001,alias,archive_accession_id,sample_id from biosamples where ds_id = 2000;
-- insert into biosamples_metadata (chip_id, chip_type, md_name,md_value,ds_id) select bs.chip_id,bs.chip_type,md_name,md_value,2001 from biosamples_metadata as bs_md left join biosamples as bs on bs.chip_id = bs_md.chip_id where bs.ds_id = 2000;



-- remove the slow indexes on probe_expressions - updated in all databases
drop index probe_expressions_normalizedlog2score;
drop index probe_expressions_normalizedscore;
drop index probe_expressions_rawscore;
drop index probe_expressions_signal;



-- Fixing up issues with dataset ids 5029 and 5030 that had overlapping samples
-- have to run this first 
ALTER TABLE biosamples_metadata DISABLE TRIGGER ALL;
alter table biosamples_metadata drop CONSTRAINT biosamples_metadata_pkey;
ALTER TABLE biosamples_metadata ADD PRIMARY KEY (chip_type, chip_id, md_name,ds_id);
ALTER TABLE biosamples_metadata ENABLE TRIGGER ALL;

-- these files are located on ascc.qfab.org you need to run the one that isn't working
-- copy biosamples_metadata from '/home/rowlandm/biosamples_metadata_5030_from_prod.txt'
-- copy biosamples_metadata from '/home/rowlandm/biosamples_metadata_5029_from_portal_beta.txt'

-- you then have to restart the stemformatics instance to allow the new primary key to be picked up
-- you then have to run expressions/setup_all_sample_metadata
