--
-- Populates the FTS field with our search terms for speedy queries.
--
-- The full text search field is updated manually after a genome
-- annotation rebuild or update.
--
-- NOTE: Most of the FTS update / genome annotation build routines in magma/db/funcs.sql
--       are currently OBSOLETE due to modifications to genome_annotations table
--       introduced by Stemformatics.
--
-- NOTE: mgi_id for human genes is hgnc id
update genome_annotations set fts_lexemes = 
   to_tsvector(gene_id) ||
   to_tsvector(entrezgene_id) ||
   to_tsvector(mgi_id) ||
   to_tsvector(refseq_dna_id) ||
   to_tsvector(associated_gene_name) ||
   to_tsvector(associated_gene_synonym) ||
   to_tsvector(description) ||
   to_tsvector(get_autocomplete_terms(associated_gene_name)) ||
   to_tsvector(get_autocomplete_terms(associated_gene_synonym))
where gene_id is not null;

