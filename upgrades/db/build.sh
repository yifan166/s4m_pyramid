#!/bin/bash
#
# build.sh
#
# Loads the Stemformatics schema.
# 

if [ -z $TMPDIR ]; then
   TMPDIR="/tmp"
fi

function load_database_settings {
  pgloader_conf="$1"
  if [ -f "$pgloader_conf" ]; then
    while read pg_line
    do
      echo $pg_line | grep -P "^host|^port|^base|^user|^pass" > /dev/null 2>&1
      if [ $? -eq 0 ]; then
         var=`echo $pg_line | sed -r -e 's/\s*\=.*$//'`
         val=`echo $pg_line | sed -r -e 's/^\w+\s*\=\s*//'`
         eval "pg_$var=\"$val\""
      fi
    done < "$pgloader_conf"

    ## Create temporary "PGPASSFILE" for 'psql' automatic password usage.
    ## See: http://www.postgresql.org/docs/current/static/libpq-pgpass.html
    export PGPASSFILE="$TMPDIR/.pgpass"
    echo "$pg_host:$pg_port:$pg_base:$pg_user:$pg_pass" > "$PGPASSFILE"
    ## If we don't set mode 600, 'psql' will ignore our password file.
    chmod 600 $PGPASSFILE

  else
    echo "Error: pgloader configuration '$pgloader_conf' not found!"
    return 1
  fi
}

function database_exec {
  sql_input="$1"
  if [ -f "$sql_input" ]; then
    result=`cat "$sql_input" | $PSQL -U $pg_user -h $pg_host $pg_base 2>&1`
  else
    result=`echo "$sql_input" | $PSQL -U $pg_user -h $pg_host $pg_base 2>&1`
  fi
  ## psql doesn't return non-zero error codes, so we try to intercept errors here.
  echo "$result" | grep -P -i "error|fatal" > /dev/null 2>&1
  if [ "$?" -eq 0 ]; then
    echo "$result" 1>&2
    return 1
  else
    echo "$result"
  fi
  return 0
}


export SHASUM=shasum
export SHASUM_OPTS="-c"

export PSQL="psql"

export PGCONF=
export MANIFEST=MANIFEST

export check_manifest=0
export ARGS=

# Process the command line

usage() {
    echo
    echo "usage:  build.sh <opts>"
    echo 
    echo "   opts:"
    echo "      -c CONFIG    -- use CONFIG for the pgloader configuration file"
    echo "      -s           -- skip MANIFEST integrity check"
    echo
    echo "    If -c is provided but no -d, database name will be same as -c (with"
    echo "     anything following a hyphen or period in the name stripped off. "
    echo
    exit 1
}

if [ ${#@} = 0 ]; then
    usage
    exit 1
fi

until [ -z "$1" ]
do
    case $1 in 
	-c)       # alternate (explicit) config file name
            shift
	    PGCONF="$1"
	    ;;
	-s)       # skip MANIFEST check
	    echo MANIFEST check will be skipped
	    check_manifest=1
	    ;;
	 *)       # catch everything else here.
	    usage
	    exit 1
	    ;;
    esac

    shift
done

# Sanity checks ...

if [ -z "$PGCONF" ]; then
    echo You need to specify a pgloader config file for database DSN settings.
    exit 1
fi

load_database_settings "$PGCONF"
if [ $? -ne 0 ]; then
  exit 1
fi


# Make sure our executables are there.
for cmd in $PSQL; do
    `which $cmd > /dev/null 2>&1`
    if [ $? -ne 0 ]; then
	echo "can't find '$cmd'"
	exit 1
    fi
done

# Make sure we have all the files we need and do MD5 checksums on them.
if [ $check_manifest = 0 ]; then
    echo "checking the manifest ... "
    if ! $SHASUM $SHASUM_OPTS -s $MANIFEST; then
	echo "one or more files seems to have the wrong SHA1 digest:"
	$SHASUM $SHASUM_OPTS $MANIFEST | grep FAIL
	exit 1
    fi
fi

echo "Loading schema ..."

database_exec "stemformatics.sql"
if [ $? -ne 0 ]; then
    echo "There were some errors during schema load, but everything might be okay (check output)."
fi

# Define functions
if [ -f funcs.sql ]; then
  echo "defining functions ... "
  database_exec "funcs.sql"
  if [ $? -ne 0 ]; then
    echo "There were some errors during function definition, but everything might be okay (check output)"
  fi
fi 

echo "Stemformatics schema load completed."
