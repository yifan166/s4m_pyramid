-- Task #109 do not use character(100), instead use text
alter table stemformatics.gene_set_items alter column gene_id type text;
alter table stemformatics.gene_sets alter column gene_set_name type text;
alter table stemformatics.users alter column username type text;
alter table stemformatics.users alter column password type text;
alter table stemformatics.users alter column organisation type text;
alter table stemformatics.users alter column full_name type text;
alter table stemformatics.users alter column confirm_code type text;
