alter table datasets add column number_of_samples int ;
update datasets set number_of_samples = (select count(distinct bsm.chip_id) from biosamples_metadata as bsm where bsm.md_name = 'Replicate Group ID' and bsm.ds_id = datasets.id and bsm.chip_id in (select chip_id from biosamples_metadata where md_name = 'Replicate' and md_value = '1') ) ;
alter table stemformatics.users add column base_export_key text ;

create table stemformatics.dataset_download_audits (uid integer, ds_id integer, download_type text, ip_address text, date_created timestamp default current_timestamp, permission_used text);
create unique index unique_uid_ds_id_date_created on stemformatics.dataset_download_audits (uid,ds_id,date_created,download_type);
create index uid_dataset_download_audits on stemformatics.dataset_download_audits (uid);
create index date_created_permission_used_dataset_download_audits on stemformatics.dataset_download_audits (date_created,permission_used);
create index ip_address_dataset_download_audits on stemformatics.dataset_download_audits (ip_address);

create table stemformatics.export_key (uid integer, key text, date_created timestamp default current_timestamp);
create unique index unique_key on stemformatics.export_key (key);
create index uid_export_key on stemformatics.export_key (uid);
create index date_created_export_key on stemformatics.export_key (date_created);
