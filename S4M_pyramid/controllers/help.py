import logging
log = logging.getLogger(__name__)

from pylons import request, response, session, tmpl_context as c
from pylons.controllers.util import abort, redirect
from pylons import url
from pylons import config
from guide.lib.base import BaseController, render

import json

from guide.model.stemformatics import *


class HelpController(BaseController):

    @action()
    def json(self):
        return redirect(url(controller='contents', action='index'), code=404)
